import java.time.Instant;
import java.util.List;

public class Memento {

    private Cell sheep;
    private Behaviour sheepB;
    private Cell shepherd;
    private Behaviour shepB;
    private Cell wolf;
    private Behaviour wolfB;
    private Cell player;

    Memento(Character sheep, Character shep, Character w, Cell p){
        this.sheep=sheep.location;
        this.shepherd=shep.location;
        this.wolf=w.location;
        this.player=p;

        this.sheepB=sheep.behaviour;
        this.shepB=shep.behaviour;
        this.wolfB=w.behaviour;
    }

    public Behaviour getSheepB() {
        return sheepB;
    }

    public void setSheepB(Behaviour sheepB) {
        this.sheepB = sheepB;
    }

    public Behaviour getShepB() {
        return shepB;
    }

    public void setShepB(Behaviour shepB) {
        this.shepB = shepB;
    }

    public Behaviour getWolfB() {
        return wolfB;
    }

    public void setWolfB(Behaviour wolfB) {
        this.wolfB = wolfB;
    }

    public Cell getSheep() {
        return sheep;
    }

    public void setSheep(Cell sheep) {
        this.sheep = sheep;
    }

    public Cell getShepherd() {
        return shepherd;
    }

    public void setShepherd(Cell shepherd) {
        this.shepherd = shepherd;
    }

    public Cell getWolf() {
        return wolf;
    }

    public void setWolf(Cell wolf) {
        this.wolf = wolf;
    }

    public Cell getPlayer() {
        return player;
    }

    public void setPlayer(Cell player) {
        this.player = player;
    }
}
