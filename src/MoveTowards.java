import bos.NoMove;
import bos.RelativeMove;

import java.util.ArrayList;
import java.util.List;

public class MoveTowards implements Behaviour {
    Character target;
    ArrayList<Cell> bestPath=new ArrayList<Cell>();
    public MoveTowards(Character character){
        this.target = character;
    }

    public ArrayList<Cell> getBestPath() {
        return bestPath;
    }

    @Override
    public String toString(){
        return "MoveTowards("+target+")";
    }

    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        ArrayList<Cell> bestPath=stage.grid.bestPath(mover.location, target.location);
        List<RelativeMove> movesToTarget=new ArrayList<>();
        if(bestPath.size()>1){
            movesToTarget = stage.grid.movesBetween(bestPath.get(0), bestPath.get(1), mover);
        }
        if (movesToTarget.size() == 0) {
            return new NoMove(stage.grid, mover);
        } else {
            return movesToTarget.get(0);
        }
    }
}
