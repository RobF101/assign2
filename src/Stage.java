import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.Pair;
import bos.RelativeMove;

public class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    private List<Character> allCharacters;
    protected Player player;
    private Instant timeOfLastMove = Instant.now();
    private ArrayList<bos.Pair<Integer, Integer>> blockList;
    private Caretaker cT=new Caretaker(this);

    public Stage() {
        SAWReader sr = new SAWReader("data/stage1.saw");
        grid = new Grid(10, 10);
        shepherd = new Shepherd(grid.cellAtRowCol(sr.getShepherdLoc().first, sr.getShepherdLoc().second), new StandStill());
        sheep = new Sheep(grid.cellAtRowCol(sr.getSheepLoc().first, sr.getSheepLoc().second), new MoveTowards(shepherd));
        wolf = new Wolf(grid.cellAtRowCol(sr.getWolfLoc().first, sr.getWolfLoc().second), new MoveTowards(sheep));

        //Block locations
        blockList = sr.getBlockLocs();
        for (int i = 0; i < blockList.size(); i++) {
            grid.cellAtRowCol(blockList.get(i).first, blockList.get(i).second).c = new Color(151, 98, 0);
        }
        grid.createGraph();

        player = new Player(grid.getRandomCell());
        this.register(player);
        this.register(cT);

        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep);
        allCharacters.add(shepherd);
        allCharacters.add(wolf);
    }

    public void update() {
        if (sheep.location.x == sheep.location.y) {//This bit of code here is better as it more accurately reflects what the sheep's behaviour on initialization.
            sheep.setBehaviour(new StandStill());
            shepherd.setBehaviour(new MoveTowards(sheep));
        }
        if (!player.inMove()) {
            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                allCharacters.forEach((c) -> c.aiMove(this).perform());
                player.startMove();
                timeOfLastMove = Instant.now();
            }
        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        for (int i = 0; i < allCharacters.size(); i++) {
            showPaths(g, allCharacters.get(i), mouseLocation);
        }
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);
    }

    //Pathfinding stuff
    public void showPaths(Graphics g, Character c, Point mL) {
        for (int y = 0; y < 20; y++) {
            for (int x = 0; x < 20; x++) {
                if (grid.cellAtRowCol(y, x).contains(mL) && grid.cellAtRowCol(y, x).contains(c.location)) {
                    ArrayList<Cell> bestMove = new ArrayList<Cell>();
                    if (c.equals(sheep) && sheep.getBehavior().toString().equals(new MoveTowards(shepherd).toString())) {
                        bestMove = grid.bestPath(c.location, shepherd.location);
                    }
                    if (c.equals(wolf) && wolf.getBehavior().toString().equals(new MoveTowards(sheep).toString())) {
                        bestMove = grid.bestPath(c.location, sheep.location);
                    }
                    if (c.equals(shepherd) && shepherd.getBehavior().toString().equals(new MoveTowards(sheep).toString())) {
                        bestMove = grid.bestPath(c.location, sheep.location);
                    }
                    for (int i = 0; i < bestMove.size(); i++) {
                        bestMove.get(i).paint(g, true, true);
                    }
                }
            }
        }
    }

    //MementoPatternStuff

    public Memento backupLastState() {
        // create a memento object used for restoring state of game
        return new Memento(this.sheep, this.shepherd, this.wolf, this.player.location);
    }

    public void restorePreviousState(Memento m) {
        //Restores from memento object
        this.sheep.location = m.getSheep();
        this.shepherd.location = m.getShepherd();
        this.wolf.location = m.getWolf();
        this.player.location = m.getPlayer();
        this.sheep.behaviour = m.getSheepB();
        this.shepherd.behaviour = m.getShepB();
        this.wolf.behaviour = m.getWolfB();
    }
}