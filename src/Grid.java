import bos.*;

import javax.sound.midi.SysexMessage;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Grid implements GameBoard<Cell> {

    private Cell[][] cells = new Cell[20][20];

    private int x;
    private int y;

    private Point lastSeenMousePos;
    private long stillMouseTime;

    public Grid(int x, int y) {
        this.x = x;
        this.y = y;

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                cells[i][j] = new Cell(x + j * 35, y + i * 35);
            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        if (lastSeenMousePos != null && lastSeenMousePos.equals(mousePosition)) {
            stillMouseTime++;
        } else {
            stillMouseTime = 0;
        }
        doToEachCell((c) -> {
            c.paint(g, c.contains(mousePosition),false);
        });
        doToEachCell((c) -> {
            if (c.contains(mousePosition)) {
                if (stillMouseTime > 20) {
                    g.setColor(Color.YELLOW);
                    g.fillRoundRect(mousePosition.x + 20, mousePosition.y + 20, 50, 15, 3, 3);
                    g.setColor(Color.BLACK);
                    g.drawString("grass: " + c.getGrassHeight(), mousePosition.x + 20, mousePosition.y + 32);
                }
            }
        });
        lastSeenMousePos = mousePosition;
    }

    public Cell getRandomCell() {
        java.util.Random rand = new java.util.Random();
        return cells[rand.nextInt(20)][rand.nextInt(20)];
    }

    private bos.Pair<Integer, Integer> indexOfCell(Cell c) {
        for (int y = 0; y < 20; ++y) {
            for (int x = 0; x < 20; ++x) {
                if (cells[y][x] == c) {
                    return new bos.Pair(y, x);
                }
            }
        }
        return null;
    }

    public Pair<Integer, Integer> findAmongstCells(Predicate<Cell> predicate) {
        for (int y = 0; y < 20; ++y) {
            for (int x = 0; x < 20; ++x) {
                if (predicate.test(cells[y][x]))
                    return new Pair(y, x);
            }
        }
        return null;
    }

    public Optional<Pair<Integer, Integer>> safeFindAmongstCells(Predicate<Cell> predicate) {
        for (int y = 0; y < 20; ++y) {
            for (int x = 0; x < 20; ++x) {
                if (predicate.test(cells[y][x]))
                    return Optional.of(new Pair(y, x));
            }
        }
        return Optional.empty();

    }

    private void doToEachCell(Consumer<Cell> func) {
        for (int y = 0; y < 20; ++y) {
            for (int x = 0; x < 20; ++x) {
                func.accept(cells[y][x]);
            }
        }
    }

    @Override
    public Optional<Cell> below(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.first < 19)
                .map((pair) -> cells[pair.first + 1][pair.second]);
    }

    @Override
    public Optional<Cell> above(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.first > 0)
                .map((pair) -> cells[pair.first - 1][pair.second]);
    }

    @Override
    public Optional<Cell> rightOf(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.second < 19)
                .map((pair) -> cells[pair.first][pair.second + 1]);
    }

    @Override
    public Optional<Cell> leftOf(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.second > 0)
                .map((pair) -> cells[pair.first][pair.second - 1]);
    }

    public Cell cellAtRowCol(Integer row, Integer col) {
        return cells[row][col];
    }


    @Override
    public java.util.List<RelativeMove> movesBetween(Cell from, Cell to, GamePiece<Cell> mover) {
        Pair<Integer, Integer> fromIndex = findAmongstCells((c) -> c == from);
        Pair<Integer, Integer> toIndex = findAmongstCells((c) -> c == to);

        List<RelativeMove> result = new ArrayList<RelativeMove>();
        //This is basically redundant code the only things ever passed into this function are a block away from each other now I've got better path-finding.
        //But should you remove said pathfinding code the ifs prevent moving into walls
        // horizontal movement
        if (fromIndex.second <= toIndex.second) {
            for (int i = fromIndex.second; i < toIndex.second; i++) {
                if (!cellAtRowCol(fromIndex.first, i + 1).isBlocked()) {
                    result.add(new MoveRight(this, mover));
                } else {
                    return result;
                }
            }
        } else {
            for (int i = fromIndex.second; i > toIndex.second; i--) {
                if (!cellAtRowCol(fromIndex.first, i - 1).isBlocked()) {
                    result.add(new MoveLeft(this, mover));
                } else {
                    return result;
                }
            }
        }

        // vertical movement
        if (fromIndex.first <= toIndex.first) {
            for (int i = fromIndex.first; i < toIndex.first; i++) {
                if (!cellAtRowCol(i + 1, fromIndex.second).isBlocked()) {
                    result.add(new MoveDown(this, mover));
                } else {
                    return result;
                }
            }
        } else {
            for (int i = fromIndex.first; i > toIndex.first; i--) {
                if (!cellAtRowCol(i - 1, fromIndex.second).isBlocked()) {
                    result.add(new MoveUp(this, mover));
                } else {
                    return result;
                }
            }
        }
        return result;
    }

    public void createGraph() {
        for (int y = 0; y < 20; y++) {
            for (int x = 0; x < 20; x++) {
                for (int k = 0; k < 3; k++) {
                    Cell myCell = cellAtRowCol(y, x);
                    if (!myCell.isBlocked()) {
                        if (y != 0) {
                            if (!cellAtRowCol(y - 1, x).isBlocked())
                                myCell.adjCell.add(cellAtRowCol(y - 1, x));
                        }
                        if (x != 19) {
                            if (!cellAtRowCol(y, x + 1).isBlocked())
                                myCell.adjCell.add(cellAtRowCol(y, x + 1));
                        }
                        if (y != 19) {
                            if (!cellAtRowCol(y + 1, x).isBlocked())
                                myCell.adjCell.add(cellAtRowCol(y + 1, x));
                        }
                        if (x != 0) {
                            if (!cellAtRowCol(y, x - 1).isBlocked())
                                myCell.adjCell.add(cellAtRowCol(y, x - 1));
                        }
                    }
                }
            }
        }
    }

    public ArrayList<Cell> bestPath(Cell from, Cell to) {
        Queue<Cell> q = new LinkedList<Cell>();
        boolean[][] visited = new boolean[20][20];
        HashMap<Cell, ArrayList<Cell>> pathTo = new HashMap<>();
        q.add(from);
        for (int y = 0; y < 20; y++) {
            for (int x = 0; x < 20; x++) {
                pathTo.put(cellAtRowCol(y, x), new ArrayList<>());
                pathTo.get(cellAtRowCol(y, x)).add(cellAtRowCol(y, x));
            }
        }
        while (q.peek() != null) {
            if (bFS(q.poll(), to, visited, q, pathTo)) {
                ArrayList<Cell> rev = new ArrayList<Cell>();
                for (int i = pathTo.get(to).size()-1; i > -1; i--) {//Reverse the list
                    rev.add(pathTo.get(to).get(i));
                }
                return rev;
            }
        }
        return null;
    }

    private boolean bFS(Cell from, Cell to, boolean[][] visited, Queue<Cell> q, HashMap<Cell, ArrayList<Cell>> pathTo) {
        Pair<Integer, Integer> p = indexOfCell(from);
        if (visited[p.first][p.second]) {
        } else if (from.equals(to)) {
            return true;
        } else {
            visited[p.first][p.second] = true;
            Iterator vi = from.adjCell.iterator();
            while (vi.hasNext()) {
                Cell nextVertex = (Cell) vi.next();
                if(pathTo.get(nextVertex).size()<2){//This checks if there is already a path to this vertex, if this wasn't here the search will show multiple paths
                    for (int i = 0; i < pathTo.get(from).size(); i++) {
                        if (!pathTo.get(nextVertex).contains(pathTo.get(from).get(i))) {
                            pathTo.get(nextVertex).add(pathTo.get(from).get(i));
                        }
                    }
                }
                q.add(nextVertex);
            }
        }
        return false;
    }
}