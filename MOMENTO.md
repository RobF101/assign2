My solution to the Quick Save feature for COMP229 Assignment 2 is as follows.
The Memento class allows for storage of all the character cell locations, 
The Caretaker class has one Memento object with two methods, backup and restore (Save and restore) and is initialized in the stage.
When the Space key is pressed in the key press is passed from main to the observer, which notifies the stage, the caretaker calls for the stage to send it a memento object for storage.
When the 'r' key is pressed (Assuming there is a save to load from), the stage loads the new values from the caretakers stored memento object.

My solution maps the standard memento pattern as follows, the caretaker stores a memento object and the originator (in this case the stage), stores to the caretaker and loads from it.

The memento pattern is not a good solution for general saves as it is not persistent, as in it will lose the saved data as soon as the application/computer shuts down and the memory clears.
This is why saving positions/behaviours to a file would be better.