import bos.GameBoard;

public class Caretaker implements KeyObserver{
    private Memento m;
    private Stage s;
    Caretaker(Stage st){
        this.s=st;
    }
    public void backup(Memento s){
        m=new Memento(new Sheep(s.getSheep(),s.getSheepB()),new Shepherd(s.getShepherd(),s.getShepB()),new Wolf(s.getWolf(),s.getWolfB()),s.getPlayer());
    }
    public Memento restore(){
        return m;
    }
    @Override
    public void notify(char c,GameBoard<Cell> gb){
        if (c == ' ') {//Backup
            System.out.println("Backup");
            backup(s.backupLastState());
        } else if (c == 'r' && this.m!=null) {//Restore
            System.out.println("Restore");
            s.restorePreviousState(m);
        }
    }
}
